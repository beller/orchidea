Orchidea

[intelligent assisted orchestration]

Assisted orchestration can be thought as the process of searching for the best combinations of orchestral sounds to match a target sound under specified metric and constraints. Although a solution to this problem has been a long-standing request from many composers, it remains relatively unexplored because of its high complexity, requiring knowledge and understanding of both mathematical formalization and musical writing.

Orchidea is an evolution of the Orch* tools and performs static and dynamic assisted orchestration. Orchidea performs mono-objective optimization on various features and requires a little number of parameters for orchestration.

(c) Copyright 2019 by Ircam, HEM, Carmine-Emanuele Cella.